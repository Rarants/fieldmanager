FROM python
COPY . /app
WORKDIR /app
RUN pip install -r backend/requirements.txt
ENTRYPOINT ["python"]
CMD ["backend/app.py"]